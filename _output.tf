output "cluster_id" {
  description = "Cluster ID"
  value       = module.gke.name
}

output "cluster_region" {
  description = "Cluster region"
  value       = module.gke.region
}

output "cluster_expected_zone" {
  description = "Cluster expected zone"
  value       = var.zones[0]
}

