variable "project_id" {
  description = "project id"
}

variable "service_account" {
  description = "impersonate service account, must have roles/iam.serviceAccountTokenCreator"
}

variable "sa_secret_name" {
  description = "name of the service account secret file"
  default = "iac-sa-secret.json"
}

variable "region" {
  description = "region"
  default     = "europe-west6"
}

variable "zone" {
  description = "zone"
  default     = "europe-west6-c"
}

variable "env_name" {
  description = "Environment name, e.g. development, staging, production, test"
  default     = "test"
}

variable "ip_range_pods_name" {
  type        = string
  description = "The secondary ip range to use for pods"
  default     = "ip-range-pods"
}
variable "ip_range_services_name" {
  type        = string
  description = "The secondary ip range to use for services"
  default     = "ip-range-services"
}
variable "zones" {
  type        = list(string)
  description = "The project ID to host the cluster in"
  default     = ["europe-west6-c"]
}

# modules

variable "cluster_name_suffix" {
  type        = string
  description = "A suffix to append to the default cluster name"
  default     = "test"
}

variable "network" {
  type        = string
  description = "The VPC network created to host the cluster in"
  default     = "gke-network"
}

variable "subnetwork" {
  type        = string
  description = "The subnetwork created to host the cluster in"
  default     = "gke-subnet"
}

variable "cluster_autoscaling" {
  type = object({
    enabled             = bool
    autoscaling_profile = string
    min_cpu_cores       = number
    max_cpu_cores       = number
    min_memory_gb       = number
    max_memory_gb       = number
    gpu_resources       = list(object({
      resource_type = string
      minimum       = number
      maximum       = number
    }))
    auto_repair  = bool
    auto_upgrade = bool
  })
  default = {
    enabled             = false
    autoscaling_profile = "BALANCED"
    max_cpu_cores       = 0
    min_cpu_cores       = 0
    max_memory_gb       = 0
    min_memory_gb       = 0
    gpu_resources       = []
    auto_repair         = true
    auto_upgrade        = true
  }
  description = "Cluster autoscaling configuration. See [more details](https://cloud.google.com/kubernetes-engine/docs/reference/rest/v1beta1/projects.locations.clusters#clusterautoscaling)"
}