locals {
  cluster_type = "node-pool"
}

# see reference doc:
# https://registry.terraform.io/modules/terraform-google-modules/kubernetes-engine/google/latest
module "gke" {
  source                               = "terraform-google-modules/kubernetes-engine/google"
  project_id                           = var.project_id
  name                                 = "${local.cluster_type}-cluster${var.cluster_name_suffix}"
  regional                             = false
  region                               = var.region
  zones                                = var.zones
  network                              = module.gcp-network.network_name
  subnetwork                           = module.gcp-network.subnets_names[0]
  ip_range_pods                        = var.ip_range_pods_name
  ip_range_services                    = var.ip_range_services_name
  create_service_account               = false
  remove_default_node_pool             = true
  disable_legacy_metadata_endpoints    = false
  cluster_autoscaling                  = var.cluster_autoscaling
  deletion_protection                  = false
  http_load_balancing                  = true
  monitoring_enable_managed_prometheus = false
  enable_shielded_nodes = false


  node_pools = [
    {
      name         = "pool-02"
      # Nodes
      image_type   = "COS_CONTAINERD"
      machine_type = "n1-standard-4"
      disk_type    = "pd-standard"
      disk_size_gb = 100

      node_locations     = "${var.region}-c"
      autoscaling        = true
      min_count          = 1 # 2
      max_count          = 3 # 4
      node_count         = 1 # 2
      #Management
      auto_upgrade       = true
      auto_repair        = true
      strategy           = "SURGE"
      service_account    = var.service_account
      cpu_manager_policy = "static"
      cpu_cfs_quota      = true
      preemptible        = true
      # Security
      sandbox_enabled    = false
      enable_secure_boot = false

    },
  ]

  #  node_pools_taints = {
  #    all = [
  #      {
  #        key    = "all-pools-example"
  #        value  = true
  #        effect = "PREFER_NO_SCHEDULE"
  #      }
  #    ]
  #  }
  #
  #  node_pools_tags = {
  #    all = [
  #      "all-node-example",
  #    ]
  #  }
  #
  #  node_pools_linux_node_configs_sysctls = {
  #    all = {
  #      "net.core.netdev_max_backlog" = "10000"
  #    }
  #  }
}