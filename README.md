# Provision of test infrastructure on GCP with Terraform and GitLab 
## Advanced Cloud Computing, W4B-C-CC001
### Daniel Vontobel, 2023-24

Example codebase only, not runnable.

## To use
* [ ] Create a project space on your google cloud platform
* [ ] Create an iac service account with editor rights, named `"iac-sa@<your-project>.iam.gserviceaccount.com"`
* [ ] Create and download a key for iac service account, store it in the root of this source, named as `iac-sa-secret.json`
* [ ] Create a cloud storage bucket named `acc-test-tf-state-bucket`

## Prepare the gcloud local use  

Open a gcloud-cli and run the script.
* [ ] Existing file ./iac-sa-secret.json is required

```bash
./config-gcloud-context
```

## Run terraform local

```bash
terraform init
```

```bash
terraform plan
``` 
You will be asked for the `project_id` and the `service_account` or you can save these in your `./personal.auto.tfvars`.

```bash
terraform apply
```
You will be asked for the `project_id` and the `service_account` or you can save these in your `./personal.auto.tfvars`.

```bash
terraform destroy
```
You will be asked for the `project_id` and the `service_account` or you can save these in your `./personal.auto.tfvars`.

## Run on GitLab

Setup your own GitLab project and rename the file `_.gitlab-ci.yml` to `.gitlab-ci.yml`.
Commit and push your project.

