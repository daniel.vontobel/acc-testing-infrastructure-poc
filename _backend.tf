

terraform {
  backend "gcs" {
    bucket                      = "acc-test-tf-state-bucket" # GCS bucket name to store terraform tfstate
    prefix                      = "tf-state-t1"              # folder name in the bucket. A bucket can contains multiple terraform project states
    impersonate_service_account = "iac-sa@<your-project>.iam.gserviceaccount.com" # must have roles/iam.serviceAccountTokenCreator
  }
}
